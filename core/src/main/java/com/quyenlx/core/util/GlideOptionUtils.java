package com.quyenlx.core.util;

import android.annotation.SuppressLint;

import com.bumptech.glide.request.RequestOptions;
import com.quyenlx.core.R;

/**
 * Created by lxquy on 1/18/2018.
 */

public class GlideOptionUtils {
    @SuppressLint("CheckResult")
    public static RequestOptions fitCenter() {
        RequestOptions options = new RequestOptions();
        options.fitCenter();
        return options;
    }

    @SuppressLint("CheckResult")
    public static RequestOptions centerCrop() {
        RequestOptions options = new RequestOptions();
        options.centerCrop();
//        options.error(R.drawable.noavatar);
        return options;
    }
}
