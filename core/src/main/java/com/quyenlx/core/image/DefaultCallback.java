package com.quyenlx.core.image;

/**
 * Created by lxquy on 1/19/2018.
 */

public abstract class DefaultCallback implements Constants.Callbacks {
    @Override
    public void onImagePickerError(Exception e, Constants.ImageSource source, int type) {

    }

    @Override
    public void onCanceled(Constants.ImageSource source, int type) {

    }
}
