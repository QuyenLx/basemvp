package com.quyenlx.core.image;

import android.support.annotation.NonNull;

import com.quyenlx.core.BuildConfig;

import java.io.File;
import java.util.List;

/**
 * Created by lxquy on 1/19/2018.
 */

public interface Constants {
    String DEFAULT_FOLDER_NAME = "EasyImage";

    interface RequestCodes {
        int EASYIMAGE_IDENTIFICATOR = 0b1101101100; //876
        int SOURCE_CHOOSER = 1 << 14;

        int PICK_PICTURE_FROM_DOCUMENTS = EASYIMAGE_IDENTIFICATOR + (1 << 11);
        int PICK_PICTURE_FROM_GALLERY = EASYIMAGE_IDENTIFICATOR + (1 << 12);
        int TAKE_PICTURE = EASYIMAGE_IDENTIFICATOR + (1 << 13);
    }

    interface BundleKeys {
        String FOLDER_NAME = BuildConfig.APPLICATION_ID + ".folder_name";
        String ALLOW_MULTIPLE = BuildConfig.APPLICATION_ID + ".allow_multiple";
        String COPY_TAKEN_PHOTOS = BuildConfig.APPLICATION_ID + ".copy_taken_photos";
        String COPY_PICKED_IMAGES = BuildConfig.APPLICATION_ID + ".copy_picked_images";

        String KEY_PHOTO_URI = BuildConfig.APPLICATION_ID + ".photo_uri";
        String KEY_LAST_CAMERA_PHOTO = BuildConfig.APPLICATION_ID + ".last_photo";
        String KEY_TYPE = BuildConfig.APPLICATION_ID + ".type";
    }

    interface Callbacks {
        void onImagePickerError(Exception e, ImageSource source, int type);

        void onImagesPicked(@NonNull List<File> imageFiles, ImageSource source, int type);

        void onCanceled(ImageSource source, int type);
    }

    enum ImageSource {
        GALLERY, DOCUMENTS, CAMERA
    }
}
