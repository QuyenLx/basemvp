package com.quyenlx.core.image;

import android.content.Context;

import com.quyenlx.core.util.SharedPrefsUtils;

/**
 * Created by lxquy on 1/19/2018.
 */

public class ImageConfiguration implements Constants {
    private Context context;

    ImageConfiguration(Context context) {
        this.context = context;
    }

    public ImageConfiguration setImagesFolderName(String folderName) {
        SharedPrefsUtils.setStringPreference(context, BundleKeys.FOLDER_NAME, folderName);
        return this;
    }

    public ImageConfiguration setAllowMultiplePickInGallery(boolean allowMultiple) {
        SharedPrefsUtils.setBooleanPreference(context, BundleKeys.ALLOW_MULTIPLE, allowMultiple);
        return this;
    }

    public ImageConfiguration setCopyTakenPhotosToPublicGalleryAppFolder(boolean copy) {
        SharedPrefsUtils.setBooleanPreference(context, BundleKeys.COPY_TAKEN_PHOTOS, copy);
        return this;
    }

    public ImageConfiguration setCopyPickedImagesToPublicGalleryAppFolder(boolean copy) {
        SharedPrefsUtils.setBooleanPreference(context, BundleKeys.COPY_PICKED_IMAGES, copy);
        return this;
    }

    public String getFolderName() {
        String name = SharedPrefsUtils.getStringPreference(context, BundleKeys.FOLDER_NAME);
        if (name == null) {
            name = DEFAULT_FOLDER_NAME;
        }
        return name;
    }

    public boolean allowsMultiplePickingInGallery() {
        return SharedPrefsUtils.getBooleanPreference(context, BundleKeys.ALLOW_MULTIPLE, false);
    }

    public boolean shouldCopyTakenPhotosToPublicGalleryAppFolder() {
        return SharedPrefsUtils.getBooleanPreference(context, BundleKeys.COPY_TAKEN_PHOTOS, false);
    }

    public boolean shouldCopyPickedImagesToPublicGalleryAppFolder() {
        return SharedPrefsUtils.getBooleanPreference(context, BundleKeys.COPY_PICKED_IMAGES, false);
    }
}
