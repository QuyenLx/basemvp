package com.quyenlx.core.dialog;

import android.animation.Animator;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.quyenlx.core.Techniques;
import com.quyenlx.core.util.AnimationUtils;
import com.quyenlx.core.util.StatusBarUtils;

/**
 * Created by lxquy on 1/18/2018.
 */

public abstract class BaseDialog<T extends BaseDialog<T>> extends Dialog {
    protected String mTag;
    protected Context mContext;
    protected DisplayMetrics mDisplayMetrics;
    protected LinearLayout mLlTop;
    protected LinearLayout mLlControlHeight;
    protected View mOnCreateView;

    protected float mWidthScale = 1;
    protected float mHeightScale;
    protected float mMaxHeight;
    protected boolean mCancel;

    private boolean mIsShowAnim;
    private boolean mIsDismissAnim;
    private boolean mIsPopupStyle;
    private boolean mAutoDismiss;
    private long mAutoDismissDelay = 1500;

    private Techniques mShowAnim;
    private Techniques mDismissAnim;

    private Handler mHandler = new Handler(Looper.getMainLooper());

    public BaseDialog(@NonNull Context context) {
        super(context);
        setDialogTheme();
        mContext = context;
        mTag = getClass().getSimpleName();
        setCanceledOnTouchOutside(true);
    }

    public BaseDialog(Context context, boolean mIsPopupStyle) {
        this(context);
        this.mIsPopupStyle = mIsPopupStyle;
    }

    private void setDialogTheme() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    public abstract View onCreateView();

    public void onViewCreated(View inflate) {

    }

    public abstract void setUiBeforShow();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mDisplayMetrics = mContext.getResources().getDisplayMetrics();
        mMaxHeight = mDisplayMetrics.heightPixels - StatusBarUtils.getHeight(mContext);

        mLlTop = new LinearLayout(mContext);
        mLlTop.setGravity(Gravity.CENTER);

        mLlControlHeight = new LinearLayout(mContext);
        mLlControlHeight.setOrientation(LinearLayout.VERTICAL);

        mOnCreateView = onCreateView();
        mLlControlHeight.addView(mOnCreateView);
        mLlTop.addView(mLlControlHeight);
        onViewCreated(mOnCreateView);

        if (mIsPopupStyle) {
            setContentView(mLlTop, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
        } else {
            setContentView(mLlTop, new LinearLayout.LayoutParams(mDisplayMetrics.widthPixels, (int) mMaxHeight));
        }

        mLlTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCancel) {
                    dismiss();
                }
            }
        });

        mOnCreateView.setClickable(true);
    }

    public View getCreateView() {
        return mOnCreateView;
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        setUiBeforShow();

        int width;
        if (mWidthScale == 0) {
            width = LinearLayout.LayoutParams.WRAP_CONTENT;
        } else {
            width = (int) (mDisplayMetrics.widthPixels * mWidthScale);
        }

        int height;
        if (mHeightScale == 0) {
            height = LinearLayout.LayoutParams.WRAP_CONTENT;
        } else if (mHeightScale == 1) {
            height = (int) mMaxHeight;
        } else {
            height = (int) (mMaxHeight * mHeightScale);
        }

        mLlControlHeight.setLayoutParams(new LinearLayout.LayoutParams(width, height));

        if (mShowAnim != null) {
            AnimationUtils
                    .with(mShowAnim)
                    .listenr(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {
                            mIsShowAnim = true;
                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {
                            mIsShowAnim = false;
                            delayDismiss();
                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {
                            mIsShowAnim = false;
                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    })
                    .playOn(mLlControlHeight);
        } else {
            AnimationUtils.reset(mLlControlHeight);
            delayDismiss();
        }
    }

    @Override
    public void setCanceledOnTouchOutside(boolean cancel) {
        this.mCancel = cancel;
        super.setCanceledOnTouchOutside(cancel);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (mDismissAnim != null) {
            AnimationUtils
                    .with(mDismissAnim)
                    .listenr(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {
                            mIsDismissAnim = true;
                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {
                            mIsDismissAnim = false;
                            superDismiss();
                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {
                            mIsDismissAnim = false;
                            superDismiss();
                        }
                    }).playOn(mLlControlHeight);
        } else {
            superDismiss();
        }
    }

    public void superDismiss() {
        super.dismiss();
    }

    public void show(int animStyle) {
        Window window = getWindow();
        window.setWindowAnimations(animStyle);
        show();
    }

    public void showAtLocation(int gravity, int x, int y) {
        if (mIsPopupStyle) {
            Window window = getWindow();
            WindowManager.LayoutParams params = window.getAttributes();
            window.setGravity(gravity);
            params.x = x;
            params.y = y;
        }
        show();
    }

    public void showAtLocation(int x, int y) {
        int gravity = Gravity.LEFT | Gravity.TOP;
        showAtLocation(gravity, x, y);
    }

    public T dimEnabled(boolean isDimEnabled) {
        if (isDimEnabled) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }
        return (T) this;
    }

    public T widthScale(float widthScale) {
        this.mWidthScale = widthScale;
        return (T) this;
    }

    public T heightScale(float heightScale) {
        mHeightScale = heightScale;
        return (T) this;
    }

    public T showAnim(Techniques showAnim) {
        mShowAnim = showAnim;
        return (T) this;
    }

    public T dismissAnim(Techniques dismissAnim) {
        mDismissAnim = dismissAnim;
        return (T) this;
    }

    public T autoDismiss(boolean autoDismiss) {
        mAutoDismiss = autoDismiss;
        return (T) this;
    }

    public T autoDismissDelay(long autoDismissDelay) {
        mAutoDismissDelay = autoDismissDelay;
        return (T) this;
    }

    private void delayDismiss() {
        if (mAutoDismiss && mAutoDismissDelay > 0) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dismiss();
                }
            }, mAutoDismissDelay);
        }
    }

    @Override
    public boolean dispatchTouchEvent(@NonNull MotionEvent ev) {
//        if(mIsDismissAnim || mIsShowAnim || mAutoDismiss){
//            return true;
//        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onBackPressed() {
//        if (mIsDismissAnim || mIsShowAnim || mAutoDismiss) {
//            return;
//        }
        super.onBackPressed();
    }
    protected int dp2px(float dp) {
        final float scale = mContext.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }
}
