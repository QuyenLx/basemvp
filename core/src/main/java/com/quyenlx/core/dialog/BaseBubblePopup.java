package com.quyenlx.core.dialog;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.quyenlx.core.R;
import com.quyenlx.core.Techniques;
import com.quyenlx.core.util.CornerUtils;
import com.quyenlx.core.util.StatusBarUtils;
import com.quyenlx.core.view.TriangleView;

/**
 * Created by lxquy on 1/18/2018.
 */

public abstract class BaseBubblePopup<T extends BaseBubblePopup<T>> extends InternalBasePopup<T> {
    protected View mWrappedView;
    protected LinearLayout mLlContent;
    protected TriangleView mTriangleView;
    protected RelativeLayout.LayoutParams mLayoutParams;
    protected int mBubbleColor;
    protected int mCornerRadius;
    protected int mMarginLeft;
    protected int mMarginRight;
    protected int triangleWidth;
    protected int triangleHeight;
    private RelativeLayout.LayoutParams mTriangleLayoutParams;

    public BaseBubblePopup(Context context) {
        super(context);
        mWrappedView = onCreateBubbleView();
        init();
    }

    public BaseBubblePopup(Context context, View wrappedView) {
        super(context);
        mWrappedView = wrappedView;
        init();
    }

    private void init() {
        showAnim(Techniques.BounceLeft);
        dismissAnim(Techniques.FadeExit);
        dimEnabled(false);

        bubbleColor(Color.parseColor("#BB000000"));
        cornerRadius(5);
        margin(8, 8);
        gravity(Gravity.TOP);
        triangleWidth(24);
        triangleHeight(12);
    }

    public abstract View onCreateBubbleView();

    @Override
    public View onCreateView() {
        View inflate = View.inflate(mContext, R.layout.popup_bubble, null);
        mLlContent = (LinearLayout) inflate.findViewById(R.id.ll_content);
        mTriangleView = (TriangleView) inflate.findViewById(R.id.triangle_view);
        mLlContent.addView(mWrappedView);

        mLayoutParams = (RelativeLayout.LayoutParams) mLlContent.getLayoutParams();
        mTriangleLayoutParams = (RelativeLayout.LayoutParams) mTriangleView.getLayoutParams();
        inflate.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        return inflate;
    }

    @Override
    public void setUiBeforShow() {
        mLlContent.setBackgroundDrawable(
                CornerUtils.cornerDrawable(Color.parseColor("#00000000"), mCornerRadius));
        mLayoutParams.setMargins(mMarginLeft, 0, mMarginRight, 0);
        mLlContent.setLayoutParams(mLayoutParams);

        mTriangleView.setColor(mBubbleColor);
        mTriangleView.setGravity(mGravity == Gravity.TOP ? Gravity.BOTTOM : Gravity.TOP);

        mTriangleLayoutParams.width = triangleWidth;
        mTriangleLayoutParams.height = triangleHeight;
        mTriangleView.setLayoutParams(mTriangleLayoutParams);
    }

    @Override
    public void onLayoutObtain() {
        mTriangleView.setX(mX - mTriangleView.getWidth() / 2);

        if (mGravity == Gravity.TOP) {
            int y = mY - mTriangleView.getHeight();
            mTriangleView.setY(y);
            mLlContent.setY(y - mLlContent.getHeight());
        } else {
            mTriangleView.setY(mY);
            mLlContent.setY(mY + mTriangleView.getHeight());
        }

        /**
         * mX--->三角形中心距离屏幕左边距离
         * mDisplayMetrics.widthPixels - mX--->三角形中心距离屏幕右边距离
         */
        int availableLeft = mX - mLayoutParams.leftMargin;//左侧最大可用距离
        int availableRight = mDisplayMetrics.widthPixels - mX - mLayoutParams.rightMargin;//右侧最大可用距离

        int x = 0;
        int contentWidth = mLlContent.getWidth();
        if (contentWidth / 2 <= availableLeft && contentWidth / 2 <= availableRight) {
            x = mX - contentWidth / 2;
        } else {
            if (availableLeft <= availableRight) {//三角形在屏幕中心的左边
                x = mLayoutParams.leftMargin;
            } else {//三角形在屏幕中心的右边
                x = mDisplayMetrics.widthPixels - (contentWidth + mLayoutParams.rightMargin);
            }
        }
        mLlContent.setX(x);
    }

    @Override
    public T anchorView(View anchorView) {
        if (anchorView != null) {
            mAnchorView = anchorView;
            int[] location = new int[2];
            mAnchorView.getLocationOnScreen(location);

            mX = location[0] + anchorView.getWidth() / 2;
            if (mGravity == Gravity.TOP) {
                mY = location[1] - StatusBarUtils.getHeight(mContext)
                        - dp2px(1);
            } else {
                mY = location[1] - StatusBarUtils.getHeight(mContext)
                        + anchorView.getHeight() + dp2px(1);
            }
        }
        return (T) this;
    }

    public T bubbleColor(int bubbleColor) {
        mBubbleColor = bubbleColor;
        return (T) this;
    }

    public T cornerRadius(float cornerRadius) {
        mCornerRadius = dp2px(cornerRadius);
        return (T) this;
    }

    public T margin(float marginLeft, float marginRight) {
        mMarginLeft = dp2px(marginLeft);
        mMarginRight = dp2px(marginRight);
        return (T) this;
    }

    public T triangleWidth(float width) {
        triangleWidth = dp2px(width);
        return (T) this;
    }

    public T triangleHeight(float height) {
        triangleHeight = dp2px(height);
        return (T) this;
    }
}