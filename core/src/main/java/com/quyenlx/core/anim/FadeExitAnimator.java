package com.quyenlx.core.anim;

import android.animation.ObjectAnimator;
import android.view.View;

import com.quyenlx.core.BaseViewAnimator;

/**
 * Created by lxquy on 1/18/2018.
 */

public class FadeExitAnimator extends BaseViewAnimator {
    @Override
    protected void prepare(View target) {
        getAnimatorAgent().playTogether(//
                ObjectAnimator.ofFloat(target, "alpha", 1, 0));
    }
}
