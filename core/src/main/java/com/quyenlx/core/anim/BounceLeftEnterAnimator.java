package com.quyenlx.core.anim;

import android.animation.ObjectAnimator;
import android.util.DisplayMetrics;
import android.view.View;

import com.quyenlx.core.BaseViewAnimator;

/**
 * Created by lxquy on 1/18/2018.
 */

public class BounceLeftEnterAnimator extends BaseViewAnimator {
    @Override
    protected void prepare(View target) {
        DisplayMetrics dm = target.getContext().getResources().getDisplayMetrics();
        getAnimatorAgent().playTogether(
                ObjectAnimator.ofFloat(target, "alpha", 0, 1, 1, 1),
                ObjectAnimator.ofFloat(target, "translationX", -250 * dm.density, 30, -10, 0)
        );
    }
}
