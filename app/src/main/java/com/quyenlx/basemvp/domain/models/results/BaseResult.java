package com.quyenlx.basemvp.domain.models.results;

import com.google.gson.annotations.SerializedName;
import com.quyenlx.basemvp.app.base.BaseModel;

/**
 * Created by quyenlx on 10/9/2017.
 */

public class BaseResult<T extends BaseModel> {
    @SerializedName("data")
    private T data;
}
