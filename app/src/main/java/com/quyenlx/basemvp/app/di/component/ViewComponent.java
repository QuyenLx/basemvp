package com.quyenlx.basemvp.app.di.component;

import com.quyenlx.basemvp.app.di.UserScope;
import com.quyenlx.basemvp.ui.fragments.tab.HomeFragment;

import dagger.Subcomponent;

/**
 * Created by quyenlx on 10/5/2017.
 */
@UserScope
@Subcomponent()
public interface ViewComponent {
    void injectTo(HomeFragment fragment);
}
