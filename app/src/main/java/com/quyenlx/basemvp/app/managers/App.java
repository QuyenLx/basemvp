package com.quyenlx.basemvp.app.managers;

import android.app.Application;

import com.quyenlx.basemvp.BuildConfig;
import com.quyenlx.basemvp.app.di.component.ApplicationComponent;
import com.quyenlx.basemvp.app.di.component.DaggerApplicationComponent;
import com.quyenlx.basemvp.app.di.component.ViewComponent;
import com.quyenlx.basemvp.app.di.module.ApplicationModule;

import timber.log.Timber;

/**
 * Created by quyenlx on 10/5/2017.
 */

public class App extends Application {
    private static App instance;
    private ApplicationComponent component;

    public static synchronized App get() {
        return instance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        component = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getComponent() {
        return component;
    }

}
