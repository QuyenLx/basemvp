package com.quyenlx.basemvp.app.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by quyenlx on 10/5/2017.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface UserScope {
}
