package com.quyenlx.basemvp.app.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.quyenlx.basemvp.app.di.component.ViewComponent;
import com.quyenlx.basemvp.app.managers.App;
import com.quyenlx.core.BasePresenter;
import com.quyenlx.core.BaseView;
import com.quyenlx.core.util.ToastUtils;

import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by quyenlx on 10/5/2017.
 */

public abstract class BaseFragment extends Fragment implements BaseView {
    protected abstract int getLayoutRes();

    protected abstract void init();

    protected View rootView;
    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(getLayoutRes(), container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }


    @Override
    public void onShowLoading(@NonNull String... arrString) {
        onHideLoading();
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(false);
        if (arrString.length == 1) {
            progressDialog.setMessage(arrString[0]);
        } else {
            progressDialog.setTitle(arrString[0]);
            progressDialog.setMessage(arrString[1]);
        }
        progressDialog.show();
    }

    @Override
    public void onHideLoading() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onRequestFailure(String message, Throwable throwable) {
        Timber.e(throwable);
        ToastUtils.showShortToast(getContext(), message);
    }

}
