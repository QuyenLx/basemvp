package com.quyenlx.basemvp.app.utils;

/**
 * Created by quyenlx on 10/5/2017.
 */

public interface Config {

    interface Network {
        int TIMEOUT_CONNECT = 120;   //In seconds
        int TIMEOUT_READ = 120;   //In seconds
        int TIMEOUT_WRITE = 120;   //In seconds
    }

    interface Enviroment {
        String DEV = "http://www.google.com";
    }
}
