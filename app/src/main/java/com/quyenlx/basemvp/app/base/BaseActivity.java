package com.quyenlx.basemvp.app.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.quyenlx.basemvp.R;

/**
 * Created by quyenlx on 10/5/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    protected abstract int getLayoutRes();

    protected abstract int getContainer();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutRes());
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
    }

    protected void replaceFragment(BaseFragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(getContainer(), fragment)
                .addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }

    protected void addFragment(BaseFragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(getContainer(), fragment)
                .addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
