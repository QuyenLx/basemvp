package com.quyenlx.basemvp.app.network;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by quyenlx on 10/5/2017.
 */

public class MyServiceInterceptor implements Interceptor {
    private String sessionToken;

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();
        Request.Builder requestBuilder = request.newBuilder();
        requestBuilder.method(request.method(), request.body());
        return chain.proceed(requestBuilder.build());
    }

}