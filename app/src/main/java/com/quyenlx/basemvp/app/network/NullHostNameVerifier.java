package com.quyenlx.basemvp.app.network;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import timber.log.Timber;

/**
 * Created by quyenlx on 10/5/2017.
 */

public class NullHostNameVerifier implements HostnameVerifier {

    @Override
    public boolean verify(String hostname, SSLSession session) {
        Timber.i("Approving certificate for " + hostname);
        return true;
    }

}