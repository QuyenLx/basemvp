package com.quyenlx.basemvp.app.base;

import com.quyenlx.core.BasePresenter;
import com.quyenlx.core.BaseView;

/**
 * Created by quyenlx on 10/5/2017.
 */

public class BasePresenterImpl<T extends BaseView> implements BasePresenter<T> {
    protected T view;

    @Override
    public void onAttachView(T view) {
        this.view = view;
    }

    @Override
    public void onDetach() {
        view = null;
    }
}
