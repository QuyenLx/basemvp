package com.quyenlx.basemvp.app.di.component;

import com.quyenlx.basemvp.app.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by quyenlx on 10/5/2017.
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    ViewComponent plus();
}
