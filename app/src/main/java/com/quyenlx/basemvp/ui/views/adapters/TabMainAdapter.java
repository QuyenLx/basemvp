package com.quyenlx.basemvp.ui.views.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.quyenlx.basemvp.ui.fragments.tab.HomeFragment;
import com.quyenlx.basemvp.ui.fragments.tab.NotifyFragment;
import com.quyenlx.basemvp.ui.fragments.tab.SettingFragment;

/**
 * Created by quyenlx on 10/6/2017.
 */

public class TabMainAdapter extends FragmentPagerAdapter {
    private HomeFragment homeFragment;
    private NotifyFragment notifyFragment;
    private SettingFragment settingFragment;

    public TabMainAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                if (homeFragment == null) {
                    homeFragment = new HomeFragment();
                }
                return homeFragment;
            case 1:
                if (notifyFragment == null) {
                    notifyFragment = new NotifyFragment();
                }
                return notifyFragment;
            case 2:
                if (settingFragment == null) {
                    settingFragment = new SettingFragment();
                }
                return settingFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
