package com.quyenlx.basemvp.ui.fragments.tab;

import com.quyenlx.basemvp.R;
import com.quyenlx.basemvp.app.base.BaseFragment;
import com.quyenlx.core.BasePresenter;
import com.quyenlx.core.BaseView;

/**
 * Created by quyenlx on 10/6/2017.
 */

public class NotifyFragment extends BaseFragment {
    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_notify;
    }
    @Override
    protected void init() {

    }
}
