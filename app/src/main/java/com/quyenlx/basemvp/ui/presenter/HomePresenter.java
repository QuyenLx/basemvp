package com.quyenlx.basemvp.ui.presenter;

import android.content.Context;
import android.support.annotation.NonNull;

import com.quyenlx.basemvp.app.base.BasePresenterImpl;
import com.quyenlx.core.BaseView;
import com.quyenlx.core.image.ImageBuild;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by lxquy on 1/22/2018.
 */

public class HomePresenter extends BasePresenterImpl<HomePresenter.HomeView> implements ImageBuild.Callbacks {
    public interface HomeView extends BaseView {
        void onPhotoReturned(List<File> imageFiles);
    }

    private final Context context;

    @Inject
    public HomePresenter(Context context) {
        this.context = context;
    }


    @Override
    public void onImagePickerError(Exception e, ImageBuild.ImageSource source, int type) {
        Timber.e(e);
    }

    @Override
    public void onImagesPicked(@NonNull List<File> imageFiles, ImageBuild.ImageSource source, int type) {
        view.onPhotoReturned(imageFiles);
    }

    @Override
    public void onCanceled(ImageBuild.ImageSource source, int type) {
        if (source == ImageBuild.ImageSource.CAMERA) {
            File photoFile = ImageBuild.lastlyTakenButCanceledPhoto(context);
            if (photoFile != null) photoFile.delete();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ImageBuild.clearConfiguration(context);
    }
}
