package com.quyenlx.basemvp.ui.fragments;

import android.content.Intent;
import android.os.Handler;

import com.quyenlx.basemvp.R;
import com.quyenlx.basemvp.app.base.BaseFragment;
import com.quyenlx.basemvp.app.di.component.ViewComponent;
import com.quyenlx.basemvp.ui.activities.TabMainActivity;
import com.quyenlx.core.BasePresenter;
import com.quyenlx.core.BaseView;
import com.quyenlx.core.Techniques;
import com.quyenlx.core.util.AnimationUtils;

import butterknife.ButterKnife;

/**
 * Created by quyenlx on 10/5/2017.
 */

public class SplashFragment extends BaseFragment {
    private Handler handler;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_splash;
    }

    @Override
    protected void init() {
        AnimationUtils
                .with(Techniques.FadeInUp)
                .playOn(ButterKnife.findById(rootView, R.id.tvWelcome));
        handler = new Handler();
        handler.postDelayed(runnable, 1500);
    }

    private Runnable runnable = this::goToMain;

    private void goToMain() {
        Intent intent = new Intent(getContext(), TabMainActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }
}
