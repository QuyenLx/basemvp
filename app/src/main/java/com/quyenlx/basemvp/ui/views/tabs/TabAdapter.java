package com.quyenlx.basemvp.ui.views.tabs;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by quyenlx on 10/6/2017.
 */

public class TabAdapter extends RecyclerView.Adapter<TabViewHolder> {
    final List<TabInfo> tabInfoList;

    public TabAdapter(List<TabInfo> tabInfoList) {
        this.tabInfoList = tabInfoList;
    }

    @Override
    public TabViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TabViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(TabViewHolder holder, int position) {
        holder.bindData(tabInfoList.get(position));
    }

    @Override
    public int getItemCount() {
        return tabInfoList.size();
    }
}
