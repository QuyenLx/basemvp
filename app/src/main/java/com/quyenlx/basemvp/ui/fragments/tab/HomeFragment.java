package com.quyenlx.basemvp.ui.fragments.tab;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.quyenlx.basemvp.R;
import com.quyenlx.basemvp.app.base.BaseFragment;
import com.quyenlx.basemvp.app.managers.App;
import com.quyenlx.basemvp.ui.presenter.HomePresenter;
import com.quyenlx.basemvp.ui.views.adapters.ImagesAdapter;
import com.quyenlx.core.image.ImageBuild;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by quyenlx on 10/6/2017.
 */

public class HomeFragment extends BaseFragment implements HomePresenter.HomeView {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @Inject
    HomePresenter presenter;

    private ArrayList<File> photos = new ArrayList<>();
    private ImagesAdapter imagesAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.get().getComponent().plus().injectTo(this);
        presenter.onAttachView(this);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_home;
    }


    @Override
    protected void init() {
        imagesAdapter = new ImagesAdapter(getContext(), photos);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(imagesAdapter);

        ImageBuild.configuration(getContext())
                .setImagesFolderName("TestXXX")
                .setCopyTakenPhotosToPublicGalleryAppFolder(true)
                .setCopyPickedImagesToPublicGalleryAppFolder(true)
                .setAllowMultiplePickInGallery(true);
    }

    @OnClick({R.id.camera_button, R.id.documents_button, R.id.gallery_button, R.id.chooser_button, R.id.chooser_button2})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.camera_button:
                ImageBuild.openCamera(this, 0);
                break;
            case R.id.documents_button:
                ImageBuild.openDocuments(this, 0);
                break;
            case R.id.gallery_button:
                ImageBuild.openGallery(this, 0);
                break;
            case R.id.chooser_button:
                ImageBuild.openChooserWithDocuments(this, "Pick source", 0);
                break;
            case R.id.chooser_button2:
                ImageBuild.openChooserWithGallery(this, "Pick source", 0);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ImageBuild.handleActivityResult(requestCode, resultCode, data, getActivity(), presenter);
    }

    @Override
    public void onPhotoReturned(List<File> imageFiles) {
        photos.addAll(imageFiles);
        imagesAdapter.notifyDataSetChanged();
        recyclerView.scrollToPosition(photos.size() - 1);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.onDetach();
    }
}
