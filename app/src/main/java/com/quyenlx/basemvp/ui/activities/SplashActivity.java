package com.quyenlx.basemvp.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.quyenlx.basemvp.R;
import com.quyenlx.basemvp.app.base.BaseActivity;
import com.quyenlx.basemvp.ui.fragments.SplashFragment;

public class SplashActivity extends BaseActivity {

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_base;
    }

    @Override
    protected int getContainer() {
        return R.id.container;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceFragment(new SplashFragment());
    }
}
