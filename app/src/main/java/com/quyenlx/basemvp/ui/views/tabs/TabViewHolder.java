package com.quyenlx.basemvp.ui.views.tabs;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quyenlx.basemvp.R;

/**
 * Created by quyenlx on 10/6/2017.
 */

public class TabViewHolder extends RecyclerView.ViewHolder {
    public LinearLayout tab;
    public ImageView icon;
    public TextView title;

    TabViewHolder(ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tab, parent, false));
        tab = itemView.findViewById(R.id.tab);
        icon = itemView.findViewById(R.id.img_icon);
        title = itemView.findViewById(R.id.tv_title);
    }

    public void bindData(TabInfo info) {
        title.setText(info.title);
        if (info.isSelected) {
            icon.setImageResource(info.select);
            title.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.colorPrimary));
        } else {
            icon.setImageResource(info.unSelect);
            title.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.colorGrey));
        }
    }
}
