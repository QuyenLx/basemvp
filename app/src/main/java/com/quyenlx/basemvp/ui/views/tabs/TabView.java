package com.quyenlx.basemvp.ui.views.tabs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.quyenlx.basemvp.R;
import com.quyenlx.core.Techniques;
import com.quyenlx.core.util.AnimationUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by quyenlx on 10/5/2017.
 */

public class TabView extends FrameLayout {
    private View container;
    private RecyclerView recycler;
    private TabAdapter mAdapter;
    private OnTabClickedListener listener;
    private List<TabInfo> tabInfoList;

    //region constructor
    public TabView(@NonNull Context context) {
        super(context);
        init();
    }

    public TabView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TabView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    //endregion

    private void init() {
        container = LayoutInflater.from(getContext()).inflate(R.layout.layout_tabs, this, false);
        addView(container);

        initTabInfo();

        mAdapter = getAdapter();
        recycler = container.findViewById(R.id.recycler);
        recycler.setLayoutManager(new GridLayoutManager(getContext(), 3));
        recycler.setAdapter(mAdapter);
    }

    private void initTabInfo() {
        tabInfoList = new ArrayList<>();
        //Home
        tabInfoList.add(new TabInfo(
                R.drawable.icon_tabbar_family_yellow,
                R.drawable.icon_tabbar_family,
                R.string.home, true));
        //Notification
        tabInfoList.add(new TabInfo(
                R.drawable.icon_tabbar_alart_yellow,
                R.drawable.icon_tabbar_alart,
                R.string.notification, false));
        //Setting
        tabInfoList.add(new TabInfo(
                R.drawable.icon_tabbar_settings_yellow,
                R.drawable.icon_tabbar_settings,
                R.string.setting, false));

    }

    private TabAdapter getAdapter() {
        if (mAdapter == null) {
            mAdapter = createAdapter();
        }
        return mAdapter;
    }

    private TabAdapter createAdapter() {
        return new TabAdapter(tabInfoList) {
            @Override
            public void onBindViewHolder(TabViewHolder holder, int position) {
                super.onBindViewHolder(holder, position);
                holder.tab.setOnClickListener(view -> {
                    if (listener != null) listener.onTabClicked(position);
                    changeTab(view, position);
                });
            }
        };
    }

    public void changeTab(View view, int position) {
        AnimationUtils.with(Techniques.Pulse).duration(1500).playOn(view);
        if (position > tabInfoList.size()) {
            throw new IllegalArgumentException(String.format("%s greatThan %s", position, tabInfoList.size()));
        }
        for (TabInfo info : tabInfoList) {
            info.isSelected = false;
        }
        tabInfoList.get(position).isSelected = true;
        mAdapter.notifyDataSetChanged();
    }


    public void setOnTabClickedListener(OnTabClickedListener listener) {
        this.listener = listener;
    }


    public interface OnTabClickedListener {
        void onTabClicked(int position);
    }


}
