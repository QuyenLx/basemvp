package com.quyenlx.basemvp.ui.activities;

import android.os.Bundle;

import com.quyenlx.basemvp.R;
import com.quyenlx.basemvp.app.base.BaseActivity;
import com.quyenlx.basemvp.ui.views.adapters.TabMainAdapter;
import com.quyenlx.basemvp.ui.views.tabs.TabView;
import com.quyenlx.basemvp.ui.views.widgets.NonSwipeableViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TabMainActivity extends BaseActivity implements TabView.OnTabClickedListener {
    @BindView(R.id.tabs)
    TabView tabs;
    @BindView(R.id.view_pager)
    NonSwipeableViewPager viewPager;

    private TabMainAdapter mAdapter;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_tab;
    }

    @Override
    protected int getContainer() {
        return R.id.container_tab;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setupTabs();
        setupViewPager();
    }

    private void setupViewPager() {
        mAdapter = new TabMainAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mAdapter);
        viewPager.setCurrentItem(0, false);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setPagingEnabled(false);
    }

    private void setupTabs() {
        tabs.setOnTabClickedListener(this);
    }


    @Override
    public void onTabClicked(int position) {
        viewPager.setCurrentItem(position, false);
    }
}
