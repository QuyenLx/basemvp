package com.quyenlx.basemvp.ui.views.tabs;

/**
 * Created by quyenlx on 10/6/2017.
 */

public class TabInfo {
    public final int select;
    public final int unSelect;
    public final int title;
    public boolean isSelected;

    public TabInfo(int select, int unSelect, int title, boolean isSelected) {
        this.select = select;
        this.unSelect = unSelect;
        this.title = title;
        this.isSelected = isSelected;
    }
}
